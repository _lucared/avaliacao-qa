import org.json.JSONArray;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertEquals;

public class albumPage extends basePage{

    // VARIABLES
    By ListJson = By.cssSelector("body > pre");
    int albumId;
    int id;
    String title;
    String url_json;
    String thumbnailUrl;

    public albumPage(WebDriver driver) {
        super(driver);
    }

    public JSONArray arrayJason(){
        String Array = getText(ListJson);
        JSONArray jsonArray = new JSONArray(Array);
        return jsonArray;
    }

    public void checkFields() {
        JSONArray jsonArray = arrayJason();
        for (int i = 0; i < jsonArray.length(); i++) {
            if (jsonArray.getJSONObject(i).getInt("id") == 6) {
                albumId = jsonArray.getJSONObject(i).getInt("albumId");
                id = jsonArray.getJSONObject(i).getInt("id");
                title = jsonArray.getJSONObject(i).getString("title");
                url_json = jsonArray.getJSONObject(i).getString("url");
                thumbnailUrl = jsonArray.getJSONObject(i).getString("thumbnailUrl");
                break;
            }
        }
        assertEquals(albumId, 1);
        assertEquals(id, 6);
        assertEquals(title, "accusamus ea aliquid et amet sequi nemo");
        assertEquals(url_json, "https://via.placeholder.com/600/56a8c2");
        assertEquals(thumbnailUrl, "https://via.placeholder.com/150/56a8c2");
    }
}
