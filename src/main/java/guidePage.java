import org.json.JSONArray;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertEquals;

public class guidePage extends basePage{

    // VARIABLES
    By AlbumLink = By.cssSelector("div > main > ul > li:nth-child(2) > a");

    public guidePage(WebDriver driver) {
        super(driver);
    }

    public void clickAlbum() {
        click(AlbumLink);
    }
}
