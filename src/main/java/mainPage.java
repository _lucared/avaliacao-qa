import org.json.JSONArray;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import static org.junit.Assert.*;

public class mainPage extends basePage {

    // VARIABLES
    String url = " https://jsonplaceholder.typicode.com/";
    By GuideBtn = By.cssSelector("ul > li:nth-child(1) > a");

    public mainPage(WebDriver driver) {
        super(driver);
    }

    public void enterMainPage() {
        getUrl(url);
    }

    public void clickGuide() {
        click(GuideBtn);
    }
}


