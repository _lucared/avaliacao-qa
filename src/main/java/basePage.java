import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class basePage {

    protected WebDriver driver;

    public basePage(WebDriver driver){
        this.driver = driver;
    }

    public WebDriver chromeDriverConnection(){
        System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        return driver;
    }

    public String getText(By locator){
        return driver.findElement(locator).getText();
    }

    public void click(By locator){
        driver.findElement(locator).click();
    }

    public void getUrl(String url){
        driver.get(url);
    }
}
