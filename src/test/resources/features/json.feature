Feature: Validação dos dados Json

  Scenario: Validar dados json
    Given Acesso a pagina jsonplaceholder
    And clico no menu Guide
    When Clicar no link /album/1/photos e abri-lo
    And Capturar os dados exibidos em tela salvando em um array JSON
    Then deve ser validado os dados do objeto com id = 6