import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.openqa.selenium.WebDriver;


public class webTest{

    private WebDriver driver;
    basePage base;
    mainPage main;
    guidePage guide;
    albumPage album;

    @Before
    public void setUp() {
        base = new basePage(driver);
        driver = base.chromeDriverConnection();
    }

    @Given("Acesso a pagina jsonplaceholder")
    public void openjasonplaceholder(){
        main = new mainPage(driver);
        main.enterMainPage();
    }
    @And("clico no menu Guide")
    public void clickMenuGuide() {
        main = new mainPage(driver);
        main.clickGuide();
    }

    @When("Clicar no link \\/album\\/{int}\\/photos e abri-lo")
    public void clickLinkAlbum(int arg) {
        guide = new guidePage(driver);
        guide.clickAlbum();
    }

    @And("Capturar os dados exibidos em tela salvando em um array JSON")
    public void saveJsonArray() {
        album = new albumPage(driver);
        album.arrayJason();
    }

    @Then("deve ser validado os dados do objeto com id = {int}")
    public void checkJsonById(int arg) {
        album = new albumPage(driver);
        album.checkFields();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}

